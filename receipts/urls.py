from django.urls import path
from receipts.views import (
    receipt_list,
    accounts_list,
    category_list,
    create_receipt,
    create_account,
    create_category,
)

urlpatterns = [
    path("", receipt_list, name="home"),
    path("accounts/", accounts_list, name="account_list"),
    path("categories/", category_list, name="category_list"),
    path("create/", create_receipt, name="create_receipt"),
    path("accounts/create/", create_account, name="create_account"),
    path("categories/create/", create_category, name="create_category"),
]
